package edu.leipzig.BigdataPraktikum;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import edu.leipzig.BigdataPraktikum.model.*;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.runtime.client.JobExecutionException;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.Types;
import org.apache.flink.table.api.java.Slide;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class StreamingJob {
    private static final String restartTriggeredCheckString = "Restart!";
    // Graph data object
    private transient static GraphPointsConfig<Object, Object, Object> graphAccumulator = new GraphPointsConfig<>();
    private static boolean loop = true;
    private static ArrayList<Category> categories = new ArrayList<Category>();

    /**
     * Flink app executor, maintains the stream app, retry on network connection failure, and control the stream rerun
     * after configuration change.
     *
     * @throws Exception
     */
    static void run(String hostname, int port) throws Exception {
        // set up the streaming execution environment================
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //===========================================================
        /*
         * init the stream with default properties
         * */
        boolean withGrouping = false;
        String windowSize = "30.seconds";
        String slideSize = "10.seconds";
        graphAccumulator.setVersion(restartTriggeredCheckString + '|' + withGrouping + '|' + windowSize + '|' + slideSize);

        // get StreamTableEnvironment
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        /*
         * this loop is used to reinitialize the stream with new properties when the stream crash on a certain Exception
         * that we induce from the generator application, we found this way for the current homework is best to avoid
         * arbitrary killing the processes since Flink stream doesn't support any lifecycle action yet and suns forever
         * with no possibility to restart or stop.
         * */
        while (loop) {
            try {
                runStream(env, tableEnv, withGrouping, windowSize, slideSize, hostname, port);
            } catch (JobExecutionException e) {
                if ((e.getCause() != null && e.getCause() instanceof ManualTriggeredException) ||
                        (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause() instanceof ManualTriggeredException)) {
                    String[] s;
                    if (e.getCause() instanceof ManualTriggeredException) {
                        s = e.getCause().getMessage().split("\\|");
                    } else {
                        s = e.getCause().getCause().getMessage().split("\\|");
                    }
                    withGrouping = Boolean.parseBoolean(s[1]);
                    windowSize = s[2];
                    slideSize = s[3];
                    System.out.format("Restart with Grouping: %s windowSize: %s slideSize: %s",
                            withGrouping, windowSize, slideSize);
                    graphAccumulator.clear();
                    loop = true;
                    Thread.sleep(10 * 1000L);
                } else {
                    loop = true;
                    e.printStackTrace();
                    System.out.println("================= Connection Problem, Sleep then retry!=========================");
                    Thread.sleep(60 * 1000L);
                }
            } catch (Exception e) { //other exceptions, try connection again
                loop = true;
                e.printStackTrace();
                System.out.println("================= Connection Problem, Sleep then retry!=========================");
                Thread.sleep(60 * 1000L);
            }
        }
    }

    /**
     * @param env
     * @param tableEnv
     * @param withGrouping
     * @param windowSize
     * @param slideSize
     * @param hostname     socket source hostname
     * @param port         socket source port
     * @throws Exception Actually run the stream with a specified configuration.
     */
    private static void runStream(StreamExecutionEnvironment env, StreamTableEnvironment tableEnv, boolean withGrouping, String windowSize, String slideSize, String hostname, int port) throws Exception {
        loop = false;
        /*
        connects to a socket source stream
        * */
        DataStream<EventEdge<String, String>> socketStream =
                env.socketTextStream(hostname, port)
                        .map(new JSONToEdgeMap()).assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<EventEdge<String, String>>() {
                            @Override
                            public long extractAscendingTimestamp(EventEdge<String, String> element) {
                                return element.getTimestamp();
                            }
                        });
        categories.add(new Category("Developer", "10", "#CC0000"));
        categories.add(new Category("Company", "50", "#E79F13"));
        categories.add(new Category("Project", "25", "#45818E"));
        graphAccumulator.setCategories(categories);
        if (withGrouping) {
            initTableApiForGrouping(tableEnv, windowSize, slideSize, socketStream);
        } else {
            processWithStreamApi(windowSize, slideSize, socketStream);
        }
        env.execute("Flink Streaming Java API Skeleton");
    }

    /**
     * @param windowSize
     * @param slideSize
     * @param socketStream Consumes the socketStream and emits the output based on the windows/slider to the web socket connected clients.
     */
    private static void processWithStreamApi(String windowSize, String slideSize, DataStream<EventEdge<String, String>> socketStream) {
        String[] w = windowSize.split("\\.");
        long wSize = Long.parseLong(w[0]); // windowSize parsed in long from incoming frontend message
        String[] s = slideSize.split("\\.");
        long sSize = Long.parseLong(s[0]); // slideSize parsed in long from incoming frontend message
        String wUnit = w[1].toUpperCase(); // eg: "MINUTES" or "SECONDS" from incoming frontend message
        String sUnit = s[1].toUpperCase(); // eg: "MINUTES" or "SECONDS" from incoming frontend message
        socketStream.windowAll(SlidingEventTimeWindows.of(Time.of(wSize, TimeUnit.valueOf(wUnit)),
                Time.of(sSize, TimeUnit.valueOf(sUnit)))).process
                (new ProcessAllWindowFunction<EventEdge<String, String>, GraphPointsConfig, TimeWindow>() {
                    @Override
                    public void process(Context context, Iterable<EventEdge<String, String>> iterable,
                                        Collector<GraphPointsConfig> collector) throws Exception {
                        graphAccumulator.clear(); //clear graph points with each window
                        for (EventEdge<String, String> value : iterable) {
                            checkRelaunchRequest(value);
                            addToGraphAccumulator(value);
                        }
                        collector.collect(graphAccumulator);
                    }

                    private void addToGraphAccumulator(EventEdge<String, String> value) {
                        graphAccumulator.addPoint(value.getSource(), value.getSourceLabel(), 0);
                        graphAccumulator.addPoint(value.getTarget(), value.getTargetLabel(), 0);
                        graphAccumulator.addEdge(value.getSource(), value.getTarget(), value.getValue(), 1L);
                    }

                    private void checkRelaunchRequest(EventEdge<String, String> value) throws ManualTriggeredException {
                        // since we trigger a restart message with a boolean grouping value even if restart with only
                        // change on window characteristics
                        if (value.getValue().equals("false") || value.getValue().equals("true")) {
                            // tailored event to indicate stream property change from the frontend
                            loop = true;
                            /*
                             * throw an exception to force the stream to crash, add the wanted relaunch properties to
                             * the exception message.
                             *  */
                            String message = String.format("%s|%s|%s|%s",
                                    restartTriggeredCheckString,
                                    value.getValue(), // grouping: true or false
                                    value.getSourceLabel(), // window size like "5.minutes"
                                    value.getTargetLabel()); // slide size like "1.minutes"
                            graphAccumulator.setVersion(message);
                            throw new ManualTriggeredException(message);
                        }
                    }
                }).addSink(new SinkFunction<GraphPointsConfig>() {
            @Override
            public void invoke(GraphPointsConfig value, Context context) throws Exception {
                Server.sendToAll(value.toString());
            }
        });
    }

    private static void initTableApiForGrouping(StreamTableEnvironment tableEnv, String windowSize, String slideSize, DataStream<EventEdge<String, String>> socketStream) {
        // since we have differences between predefined time units among Flink APIs,
        // so we convert the special case "milliseconds" to "millis"
        if (windowSize.contains("milliseconds")) {
            windowSize = windowSize.substring(0, windowSize.length() - 6);
        }
        if (slideSize.contains("milliseconds")) {
            slideSize = slideSize.substring(0, slideSize.length() - 6);
        }
        //  Convert the DataStream into a Table with default fields "f0", "f1" ...."f6",
        //  besides extract timestamp and assign watermarks based on knowledge of the stream
        // finally we apply a grouping window on edges and vertices as requested
        Table table = tableEnv.fromDataStream(socketStream, "f0, f1, f2, f3, f4, f5, f6.rowtime");

        // edge grouping
        Table edgeTable = table.window(Slide.over(windowSize).every(slideSize).on("f6")
                .as("eventsWindow"))
                .groupBy("eventsWindow, f1, f3, f5")
                .select("f3 as sourceLabel, f5 as TargetLabel, f1 as edgeLabel , f0.count as edgeCount");

        // vertex grouping
        Table sourceTable = table.select("f0,f2 as f1,f3 as f2,f6 as f3");
        Table targetTable = table.select("f0,f4 as f1,f5 as f2,f6 as f3");

        Table vertexTable = sourceTable.unionAll(targetTable);
        DataStream<Row> result = tableEnv.toAppendStream(vertexTable, Types.ROW(Types.STRING(), Types.STRING(), Types.STRING(), Types.SQL_TIMESTAMP()));
        Table groupedVertexTable = tableEnv.fromDataStream(result, "f0, f1, f2, f3.rowtime")
                .window(Slide.over(windowSize).every(slideSize).on("f3")
                        .as("eventsWindow"))
                .groupBy("eventsWindow, f2")
                .select("f2 as vertexLabel , f0.count as vertexCount");

        // converting back edge table and vertex table to stream
        tableEnv.toAppendStream(edgeTable, Row.class).union(tableEnv.toAppendStream(groupedVertexTable, Row.class))
                .addSink(new SinkFunction<Row>() {
                    @Override
                    public void invoke(Row value, Context context) throws Exception {
                        checkRelaunchRequest(value);
                        addToGraphAccumulator(value);
                        Server.sendToAll(graphAccumulator.toString());
                    }

                    private void checkRelaunchRequest(Row value) throws ManualTriggeredException {
                        // since we trigger a restart message with a boolean grouping value
                        if (value.getArity() > 2 && (value.getField(2).equals("false") || value.getField(2).equals("true"))) {
                            loop = true;
                            String message = String.format("%s|%s|%s|%s",
                                    restartTriggeredCheckString,
                                    value.getField(2), // grouping: true or false
                                    value.getField(0), // window size like "5.minutes"
                                    value.getField(1)); // slide size like "1.minutes"
                            graphAccumulator.setVersion(message);
                            throw new ManualTriggeredException(message);
                        }
                    }

                    private void addToGraphAccumulator(Row value) {
                        if (value.getArity() > 2) { // super edge
                            graphAccumulator.addPoint(value.getField(0), value.getField(0), 0);
                            graphAccumulator.addPoint(value.getField(1), value.getField(1), 0);
                            graphAccumulator.addEdge(value.getField(0), value.getField(1), value.getField(2), ((Number) value.getField(3)).longValue());
                        } else // super vertex{}
                        {
                            graphAccumulator.addPoint(value.getField(0), value.getField(0), Integer.parseInt(value.getField(1).toString()));
                        }
                    }
                });
    }

    /**
     * Parses the JSON incoming from the generator, map it to the corresponding EventEdge instance
     */
    public static class JSONToEdgeMap implements MapFunction<String, EventEdge<String, String>> {

        JSONToEdgeMap() {
        }

        public EventEdge<String, String> map(String s) {
            JsonElement root = new JsonParser().parse(s);
            String type = root.getAsJsonObject().get("type").getAsString();
            Gson g = new Gson();
            switch (type) {
                case "COMMIT":
                    Type developerProject = new TypeToken<Action<Developer, Project>>() {
                    }.getType();
                    Action<Developer, Project> commit = g.fromJson(s, developerProject);
                    return new EventEdge<>(commit.Id, type, commit.timestamp, commit.source.name, "Developer",
                            commit.target.name, "Project");

                case "MEMBER_OF":
                    Type developerCompany = new TypeToken<Action<Developer, Company>>() {
                    }.getType();
                    Action<Developer, Company> memOf = g.fromJson(s, developerCompany);
                    return new EventEdge<>(memOf.Id, type, memOf.timestamp, memOf.source.name, "Developer",
                            memOf.target.name, "Company");

                case "BELONGS_TO":
                    Type projectCompany = new TypeToken<Action<Project, Company>>() {
                    }.getType();
                    Action<Project, Company> belongTo = g.fromJson(s, projectCompany);
                    return new EventEdge<>(belongTo.Id, type, belongTo.timestamp, belongTo.source.name, "Project",
                            belongTo.target.name, "Company");
                case "controlSignal":
                    String withGrouping = root.getAsJsonObject().get("withGrouping").getAsString();
                    String windowSize = root.getAsJsonObject().get("windowSize").getAsString();
                    String slideSize = root.getAsJsonObject().get("slideSize").getAsString();
                    long timestamp = Long.parseLong(root.getAsJsonObject().get("timestamp").getAsString());
                    // we create a restart event edge, which holds the grouping option besides the window configuration
                    return new EventEdge<>(restartTriggeredCheckString, withGrouping, timestamp, windowSize, slideSize);
                default:
                    return null;
            }
        }
    }

    /**
     * Exception to throw when a new change in stream processing properties is required from the frontend.
     */
    protected static class ManualTriggeredException extends Exception {
        private String triggeredMessage;

        ManualTriggeredException(String triggeredMessage) {
            this.triggeredMessage = triggeredMessage;
        }

        public String getMessage() {
            return triggeredMessage;
        }

    }

}
