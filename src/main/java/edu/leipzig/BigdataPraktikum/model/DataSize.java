package edu.leipzig.BigdataPraktikum.model;

public enum DataSize {
    SMALL,
    MEDIUM,
    LARGE
}
