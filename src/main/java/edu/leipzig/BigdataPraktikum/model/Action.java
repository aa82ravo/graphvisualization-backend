package edu.leipzig.BigdataPraktikum.model;

/**
 * @param <S> source type
 * @param <T> target type
 * provides a toString function that writes the action data
 * readable JSON
 */
public class Action<S, T> {
    public Long timestamp;
    public ActionType type;
    public String Id;
    public S source;
    public T target;

    @Override
    public String toString() {
        return String.format("{\"ObjectType\":\"%s\",\"type\":\"%s\", \"Id\":\"%s\", \"source\":%s, \"target\":%s, \"timestamp\":\"%s\"}", "Event", type, Id, source.toString(), target.toString(), timestamp);
    }
}
