package edu.leipzig.BigdataPraktikum.model;

import org.apache.flink.api.java.tuple.Tuple7;

/**
 * @param <K> key type
 * @param <V> value type
 *           in our example we adapted  EventEdge<String, String>
 * represents the graph edges using Flink-Tuple7
 *  besides the f0: edge Id, f1: edge label, f2: source Id, f3: source Label, f4: target Id and f5: target Label
 *  we stored f6: the timestamp of the corresponding edge
 */

public class EventEdge<K, V> extends Tuple7<String, V, K, V, K, V, Long> {

    /**
     * Default constructor is necessary to apply to POJO rules.
     */
    public EventEdge() {
    }

    /**
     *  constructor without source Id and target Id
     */
    public EventEdge(final String id, final V label, final Long timestamp, final V sourceLabel,
                     final V targetLabel) {
        this.f3 = sourceLabel;
        this.f5 = targetLabel;
        this.f1 = label;
        this.f6 = timestamp;
        this.f0 = id;
    }
    /**
     *  constructor with all fields
     */
    public EventEdge(final String id, final V label, final Long timestamp, final K sourceId,
                     final V sourceLabel, final K targetId, final V targetLabel) {
        this.f2 = sourceId;
        this.f4 = targetId;
        this.f1 = label;
        this.f6 = timestamp;
        this.f0 = id;
        this.f3 = sourceLabel;
        this.f5 = targetLabel;
    }

    public K getSource() {
        return this.f2;
    }

    public K getTarget() {
        return this.f4;
    }

    public Long getTimestamp() {
        return this.f6;
    }

    public V getValue() {
        return this.f1;
    }

    public V getSourceLabel() {
        return this.f3;
    }

    public V getTargetLabel() {
        return this.f5;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(f0).append(",");
        sb.append(f1).append(",");
        sb.append(f2).append(",");
        sb.append(f3).append(",");
        sb.append(f4).append(",");
        sb.append(f5).append(",");
        sb.append(f6).append(".");
        return sb.toString();
    }
}
