package edu.leipzig.BigdataPraktikum.model;

public class Project {
    public String name;
    public String Id;

    public Project(String Id, String name) {
        this.name = name;
        this.Id = Id;
    }

    @Override
    public String toString() {
        return String.format("{\"ObjectType\":\"%s\",\"name\":\"%s\", \"Id\":\"%s\"}", "Project", name, Id);
    }
}
