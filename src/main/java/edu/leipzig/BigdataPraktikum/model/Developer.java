package edu.leipzig.BigdataPraktikum.model;

public class Developer {
    public String name;
    public String Id;
    public String companyId;

    @Override
    public String toString() {
        return String.format("{\"ObjectType\":\"%s\",\"name\":\"%s\", \"Id\":\"%s\", \"companyId\":\"%s\"}", "Developer", name, Id, companyId);
    }
}
