package edu.leipzig.BigdataPraktikum.model;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * @param <K>  Point/ Edge key type
 * @param <EV> Edge value type
 * @param <VV> Point value type
 *             Maintains the graph points and edges, provides a toString function that writes the graph data into Frontend
 *             readable JSON
 */
public class GraphPointsConfig<K, EV, VV> {
    private HashMap<K, MyPoint<K, VV>> points = new HashMap<>();
    private HashMap<String, Edge<K, EV>> edges = new HashMap<String, Edge<K, EV>>();
    private String version = "start";
    private ArrayList<Category> categories;

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void clear() {
        this.edges.clear();
        this.points.clear();
    }

    public void addPoint(K id, VV type, int count) {
        if (this.points.get(id) == null) {
            this.points.put(id, new MyPoint<>(id, type, count));
        } else if (count > 0) {
            this.points.get(id).setCount(count);
        }
    }

    public void addEdge(K source, K target, EV label, Long value) {
        this.edges.put(String.format("%s=>%s", source, target), new Edge<>(source, target, label, value));
    }

    @Override
    public String toString() {
        if (edges.size() > 0) {
            StringBuilder points = new StringBuilder("[");
            for (Map.Entry<K, MyPoint<K, VV>> entry : this.points.entrySet()) {
                MyPoint<K, VV> v = entry.getValue();
                points.append(v.toString());
                points.append(",");
            }
            points = new StringBuilder(points.substring(0, points.length() - 1));
            points.append("]");
            StringBuilder edges = new StringBuilder("[");
            for (Map.Entry<String, Edge<K, EV>> entry : this.edges.entrySet()) {
                String k = entry.getKey();
                Edge<K, EV> v = entry.getValue();
                edges.append(v.toString());
                edges.append(",");
            }
            edges = new StringBuilder(edges.substring(0, edges.length() - 1));
            edges.append("]");
            StringBuilder categories = new StringBuilder("[");
            for (Category entry : this.categories) {
                categories.append(entry.toString());
                categories.append(",");
            }
            categories = new StringBuilder(categories.substring(0, categories.length() - 1));
            categories.append("]");
            return String.format("{\"points\":%s, \"edges\":%s, \"version\":\"%s\", \"categories\":%s}",
                    points.toString(), edges.toString(), version, categories.toString());
        } else
            return "";
    }
}

class MyPoint<K, V> {
    public K id;
    private V label;
    private int count;
    private int size;

    MyPoint(K id, V label, int count) {
        this.id = id;
        this.label = label;
        this.count = count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        if (count > 0) {
            size = count + 25;
        } else {
            switch (label.toString()) {
                case "Developer":
                    this.size = 10;
                    break;
                case "Project":
                    this.size = 25;
                    break;
                case "Company":
                    this.size = 50;
                    break;
            }
        }
        String str = (count > 0) ? String.valueOf(count) : "";
        return String.format("{\"name\":\"%s\",\"symbolSize\":\"%d\" ,\"category\":\"%s\",\"value\":\"%s\"," +
                        "\"label\": {\"normal\":{\"show\": true}}}",
                id, size, label.toString(), label.toString() + " | " + str);
    }
}

class Edge<K, V> {
    private K sourceId;
    private K targetId;
    private V label;
    private Long value;

    Edge(K source, K target, V label, Long value) {
        this.sourceId = source;
        this.targetId = target;
        this.label = label;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.
                format("{\"source\":\"%s\", \"target\":\"%s\"," +
                                " \"value\":\"%s\"," +
                                " \"label\": {\"normal\":{\"show\": true}}," +
                                " \"lineStyle\": {\"normal\":{\"width\":%d}}}"
                        , sourceId, targetId, label + " | " + value.toString(), 1);
    }
}



