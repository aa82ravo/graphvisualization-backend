package edu.leipzig.BigdataPraktikum.model;

public class Category {
    private String name;
    private String symbolSize;
    private String color;

    public Category(String name, String symbolSize, String color) {
        this.name = name;
        this.symbolSize = symbolSize;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.
                format("{\"name\":\"%s\", \"itemStyle\":{\"color\": \"%s\"}}"
                        , name, color);
    }
}
