package edu.leipzig.BigdataPraktikum;

import io.undertow.Undertow;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;

import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static io.undertow.Handlers.*;

/**
 * bootstrapper for the backend activities, it initializes the web socket server which feeds the frontend the graph data,
 * also run the Flink job.
 */
        public class Server {
            // list of the connected web socket clients
            private static ArrayList<WebSocketChannel> channels = new ArrayList<>();
            private static String webSocketListenPath = "/graphData";
            private static int webSocketListenPort = 8080;
            private static String webSocketHost = "0.0.0.0";
            private static String generatorSocketHost = "localhost";
            private static int generatorSocketPort = 6666;

            public static void main(String[] args) throws Exception {
                initWebSocketServer();
                initFlinkStream();
            }

            /**
             * runs the Flink stream processor.
             *
             * @throws Exception
             */
            private static void initFlinkStream() throws Exception {
                StreamingJob.run(generatorSocketHost, generatorSocketPort);
    }

    /**
     * runs the web socket server to run in separate  process before Flink initialization
     * also hosts the frontend
     */
    private static void initWebSocketServer() {
        Undertow server = Undertow.builder().addHttpListener(webSocketListenPort, webSocketHost)
                .setHandler(path().addPrefixPath(webSocketListenPath, websocket((exchange, channel) -> {
                    channels.add(channel);
                    channel.getReceiveSetter().set(getListener());
                    channel.resumeReceives();
                })).addPrefixPath("/", resource(new ClassPathResourceManager(Server.class.getClassLoader(),
                        Server.class.getPackage())).addWelcomeFiles("index.html")/*.setDirectoryListingEnabled(true)*/))
                .build();
        server.start();
    }

    /**
     * helper function to Undertow server
     */
    private static AbstractReceiveListener getListener() {
        return new AbstractReceiveListener() {
            @Override
            protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                final String messageData = message.getData();
                for (WebSocketChannel session : channel.getPeerConnections()) {
                    System.out.println(messageData);
                    WebSockets.sendText(messageData, session, null);
                }
            }
        };
    }

    /**
     * sends a message to the all connected web socket clients
     */
    public static void sendToAll(String message) {
        for (WebSocketChannel session : channels) {
            WebSockets.sendText(message, session, null);
        }
    }
}