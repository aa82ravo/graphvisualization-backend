\documentclass[12pt,a4paper]{article}

\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage[left=3cm,right=3cm,top=2cm,bottom=2cm]{geometry}

\usepackage{fancyhdr}
\usepackage{setspace}
\usepackage[hidelinks]{hyperref}
\usepackage {tikz}

\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[L]{\leftmark}
\fancyfoot[C]{\thepage}
\setlength{\headheight}{15pt}

\usepackage {tikz}
\usetikzlibrary {positioning}
\usetikzlibrary{shapes,arrows,shadows}
\newcommand{\mx}[1]{\mathbf{\bm{#1}}} % Matrix command
\newcommand{\vc}[1]{\mathbf{\bm{#1}}} % Vector command

\renewcommand{\baselinestretch}{1,5}
\renewcommand{\footrulewidth}{0pt}
\usepackage[italian,english]{babel}

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------
\hypersetup{pageanchor=false}
\begin{titlepage} 
\vfill
\begin{center}	
\line(1,0){400}\\[1mm]

\Huge{Processing And Visualization of Graph Streams}	
\line(1,0){400}\\[1mm]
\end{center}				
\vfill 
	%------------------------------------------------
	%	Author name and information
	%------------------------------------------------
	
	\parbox[t]{0.93\textwidth}{ % Box to inset this section slightly
		\raggedleft % Right align the text
		\large % Increase the font size
		{\Large Abdalrahman Alkamel}\\[4pt] % Extra space after name
		{\Large Rana Noureddin}\\[4pt]
		Big Data Praktikum SS 2019\\
		Supervisor: Christopher Rost\\
		Universität Leipzig\\[4pt] % Extra space before URL
		
		\hfill\rule{0.2\linewidth}{1pt}% Horizontal line, first argument width, second thickness
	}
	
\end{titlepage}
\tableofcontents
\thispagestyle{empty}
\clearpage
\hypersetup{pageanchor=true}
\setcounter{page}{1}

%----------------------------------------------------------------------------------------


\section{Introduction}
Data streams carry all sorts of data, moreover it's known that with streams comes challenges in regard with processing and comprehending the incoming massive amount of non-stopping data.
Otherwise graphs can be very handy in representing knowledge, besides their role as magnificent tool, as data modelling and analysing relationships. One cannot escape noticing the growing part that the graphs play. 
Although the Graph model has been studied, traversed, and handled well for static pictured data, still such model that come in shape of stream grams, that accumulates with time, to build a time-changing temporal graph not extensively researched.\\
Our practical task is to get one step ahead in this matter. According to that, we are planning to convert a data stream into graph within a given time. In consequence, taking advantage of the simplicity feature of graphs in representing complex interactions between real world objects.  
We consider an input data stream of events as edge stream with event time for each edge as timestamp. In addition, a grouping operator will be implemented, since we need to observe a global analytical overview of the underlying data. The derived graph model will be interpreted as input to graph visualization library.\\

From here on, the report is organized as follows: in \hyperref[subsec:Importing]{Section 2.1}, we present an overview of the selected data streams as input of our program, describing our importer and the variant of our streams.
Afterwards in \hyperref[subsec:Mapping]{Section 2.2} we introduce our theoretical Graph stream model, a closer look at the edge and vertex structure. In \hyperref[subsec:Grouping]{Section 2.3} we explain our grouping operator semantic. In \hyperref[sec:Challenges]{Section 3}, we discuss what challenges have we encountered until now. Finally, we explain the solution design in \hyperref[sec:Design] {Section 4}.

\section{Stream to Graph}
\subsection{Data Stream Importing}
\label{subsec:Importing}
As data stream, Stack Overflow temporal network has been chosen, which contains three different types of streams based on interactions represented by a directed edge (u, v, t) as follow:
\begin{itemize}
	\itemsep0em 
	\item user u answered user v's question at time t
	\item user u commented on user v's question at time t
	\item user u commented on user v's answer at time t.
\end{itemize}
where edges are separated by a new line, below is the filed description.
\begin{center}
	\begin{tabular}{ | c | c | c | } 
		 \hline	SRC & TGT & UNIXTS \\
		 \hline
		id of the source node (a user) & id of the target node (a user) & Unix timestamp \\  
		\hline
	\end{tabular}
\end{center}
this data is selected as example to work with, Flink provide multiple interfaces to consume the incoming data, and we are not limited to read those files.\\
Based on Streaming API of Apache Flink, the three previous files are added as data stream sources with assigning the timestamps of the elements as time event. As next step, a union operator is applied in order to get a unified stream with varied interactions. Since the idea of stream graph model is the real time stream event processing (here the event time is the time stamped in each record), after the merging the sorting process and assigning timestamps are imperative.
 
\subsection{Mapping Data Stream to Graph Stream Model}
\label{subsec:Mapping}
We decided to map each element of stream (u,v,t) as simple edge graph. Each edge is represented by the source Id and the target Id of the users, generated identifier and the edge label. The latter encodes what kind of the user interaction is. In this context, we have three types of edge labels: [Answer to Question, Comment to Question and Comment to Answer]. While the vertex is represented by a  generated identifier and a label that symbolizes its category as User in this case.\newline
The definition of a grouping operator semantics depends on both of the vertex label and the edge label. In our stream case to realize the grouping on vertices we decided to store fore each vertex additional information that holds, if the user answer to a question or comment either to a question or to an answer. Based on that we have three vertex types [Question, Answer, Comment], which we can group on.
\subsection{Graph Grouping}
\label{subsec:Grouping}
Although graphs provide simplicity that could be offered over data stream objects, we need more powerful analytical insights of the underlying information, such what represents summary graphs. Therefore we will implement a grouping over window algorithm, that is based on the vertex label and edge label including the aggregate function COUNT() over user-defined window. According to our stream we have one type of vertices (User), but as we mentioned before we modify our stream graph model to gain more three vertex types [Question, Answer, Comment]. Analogous to the edges, there are also three edge label types [Answer to Question, Comment to Question and Comment to Answer]. Hence the resulting summary graph will be a type graph, that changes over time, as illustrated in Figure~\ref{fig:Grouping}, which represents the count of the Stack Overflow users and count of their typed-interactions in a given time window. Note, the grouping process in this example is on vertices and edges.  
\begin{figure}
	\begin{center}
	\begin{tikzpicture}[-latex ,auto ,node distance =4 cm and 5cm ,on grid ,semithick ,state/.style ={ circle ,top color =white , draw , minimum width =1 cm}]
	\tikzstyle{vertex}=[circle, draw]
	\node[vertex, label=above:$count:120$](A) [left] {$USER$};
	\path (A) edge [loop left] node[left] {$A2Q\virgola 40$} (A);
	\path (A) edge [loop right] node[right]  {$C2Q\virgola 10$} (A);
	\path (A) edge [loop below] node[below] {$C2A\virgola 50$} (A);
\end{tikzpicture}
\end{center}
	\caption{Grouped Graph}
	\label{fig:Grouping}
\end{figure}

\section{Challenges}

\label{sec:Challenges}

Thinking of visualizing live stream data, and that the frequency of the incoming events is not bounded, one must tune the changes on the output graph that they could be comprehended by the naked human eye. and here lies the “right size of the window” as the key, but we first have to know the characteristic of the incoming data. And that for a generic visualization application is not possible.\\
Here we see the need to make the window size configurable from the user side (which is not supported in the run time by Flink) thus a way to control a mechanism like stopping the running streaming application, restart it with the window configuration as input, and also track this progress in the front end, so the user can know what is happening rather than just waiting.
sometimes also after making the window really small, still the incoming amount is too big to display, here we may need to selectively ignore some nodes depending on a hash function that cuts the excessive amount and only outputs the part that can be displayed comfortably.

\section{Project Design}
\label{sec:Design}
In order to achieve the required functionality, the connected subsystems are needed: 
\newline in Figure~\ref{fig:topology} we see the topology of the connected subsystems.

% Define the layers to draw the diagram
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

% Define block styles used later

\tikzstyle{sensor}=[draw, fill=blue!20, text width=5em, 
text centered, minimum height=2.5em,drop shadow]
\tikzstyle{ann} = [above, text width=5em, text centered]
\tikzstyle{wa} = [sensor, text width=10em, fill=red!20, 
minimum height=6em, rounded corners, drop shadow]
\tikzstyle{sc} = [sensor, text width=13em, fill=red!20, 
minimum height=10em, rounded corners, drop shadow]

% Define distances for bordering
\def\blockdist{2.3}
\def\edgedist{2.5}
\begin{figure}
	\begin{tikzpicture}
	\node (flink) [wa] {\textbf{Flink App\\}}; 
	\path (flink.east)+(\blockdist,0) node (host) [sensor] {Web hosting App};
	\path (flink.west)+(-\blockdist,0) node (handler) [sensor] {Web Socket Handler};
	\path (flink.south)+(0,-0.5)  node (serv) {Server App};
	
	\begin{pgfonlayer}{background}
	\path (handler.west |- handler.north)+(-0.5,1.5) node (a) {};
	\path (host.east |- serv.east)+(+0.5,-0.4) node (c) {};
	\path[fill=yellow!20,rounded corners, draw=black!50, dashed]
	(a) rectangle (c);           
	\path (handler.north west)+(-0.2,0.2) node (a) {};
	\end{pgfonlayer}
	
	\path (flink.south)+(-2.0,-3.5) node (graph) [wa] {\textbf{Graph Library \\}};
	\path (graph.east)+(\blockdist,0) node (mod) [sensor] {Server Control Module};
	\path (graph.west)+(-3.2,0.1) node (con) [sensor] {Web Socket Connector};
	\path (graph.south) +(0,-0.5) node (front) {Front-end Side};
	\begin{pgfonlayer}{background}
	\path (con.west)+(-0.5,1.8)node (g) {};
	\path (mod.east |- graph.south)+(0.5,-0.8)  node (h) {};
	\path[fill=yellow!20,rounded corners, draw=black!50, dashed]
	(g) rectangle (h);
	
	\path [draw, <->] (handler) edge  node [left] {TCP (web socket)} (con.north);             
	\end{pgfonlayer}
	\end{tikzpicture}
	\caption{Topology of the connected systems}
	\label{fig:topology}
\end{figure}


\subsection{The Front-end}
This part should provide the mechanism of Visualizing the graph and controlling the window size besides the Grouping. \\Client-side rendering is mandatory to reflect the rapid changes in the stream, we decided to use \href{'https://angular.io/'}{Angular framework} and a JavaScript Visualization library to achieve that.

\subsection{The Back-end}
Here runs Apache Flink as stream processing engine, we have the following components:

\subsubsection{Data Transformation}
In this context we need to provide the means by which we can consume the incoming stream, in other words, we have to supply the code that extract the data from incoming stream object. This should be accomplished by implementing an interface, specifying and overriding a map/transform function.

\subsubsection{Graph Representation}
We want at the end to display the graph, as input we have events incoming as a stream, to accomplish this we will have a global state over all stream objects that corresponds to the graph representing the current window. This state object models a way to serialize the vertices and edges regarding a schema understandable by the front-end.

\subsubsection{The Server Application}
the Front-end need to be hosted in the server and be accessible by the public. We will extend the Java application to provide this by using \href{'http://undertow.io/'}{Undertow} which is an embeddable web server with advanced API by JBoss.\\This part should provide a mechanism to allow the following features:

\begin{itemize}
    \itemsep0em 
	\item host the front-end application in a separate process which could be initialize regardless of Flink application
	\item be able to host controlling functionality over aforementioned Flink application (stopping, restarting with different window size, dispatching Flink current running status)
	\item establish a communication channel between Flink application and the client via web sockets
\end{itemize}

\subsection{The Communication Layer}
Our use case demands a bidirectional means of communication between the client side and the server side. Furthermore, the link should be capable of archiving a near real time communication speed. Web socket is a great candidate that provide such capabilities and its superior to server sent events which capable to establish a unidirectional communication link. however, web sockets may fall down when running in a controlled environment (behind firewall for example) that the port needed for TCP protocol maybe closed in counterpart to the HTTP port 80 of server sent events that is impossible to be closed in general. An implementation with server sent events is possible however but it’s not the scoop of this practice. The server application that we described ahead can host a web socket endpoint that the client will connect to it.


\end{document}
	